#include <stdio.h>
#include <string.h>
// -------------
// CONSTANTS
// -------------
#define TOTAL_CANDIDATES 7;
#define NAME_LENGTH 20;

// -------------
// STRUCTS
// -------------
struct candidate {
    char fname[15];
    char lname[15];
    char campaignSlogan[100];
    int numVotes;
};

struct electionResult {
    int totalVotes;
    int validVotes;
    int invalidVotes;
};

// -------------
// FUNCTIONS
// -------------
void showMainMenu();
void lookupCandidate(struct candidate c[]);
void viewElectionResults(struct electionResult result, struct candidate c[]);
void getCandidates(FILE * file, struct candidate candidates[]);
struct electionResult getVoteInformation(FILE *file, struct candidate candidates[]);
void printCandidate(struct candidate c);
void printAllCandidates(struct candidate c[]);
int getMostVotes(struct candidate c[]);

// -------------
// Main function
// -------------
int main() {

    // 1. open the file
    FILE * inputFile = fopen("votedata.txt","r");
    if (inputFile == NULL) {
        printf("Cannot open file.\n");
        return -1;
    }
    else {
        printf("Opened file successfully.\n");
    }

    // 2. create an array of candidates
    struct candidate candidates[7];

    // 3. process the file
    printf("Processing input file\n");
    // 3a. get the candidate information
    getCandidates(inputFile, candidates);
    // 3b. get the vote information
    struct electionResult results = getVoteInformation(inputFile, candidates);

    // 4. show menu
    int userChoice = 0;
    while (userChoice != -1) {
        showMainMenu();
        scanf("%d", &userChoice);

        if (userChoice == 1) {
            lookupCandidate(candidates);
        }
        else {
            viewElectionResults(results, candidates);
        }
    }
    printf("Done!");
    return 0;
}

void showMainMenu() {
    printf("VOTING RESULTS MENU:\n");
    printf("1. Lookup a candidate\n");
    printf("2. See election result\n");
    printf("Enter your choice (-1 to quit)\n");
}

void lookupCandidate(struct candidate c[]) {
    printf("Candidate Lookup\n");
    printf("-----------------\n");

    printf("Enter the candidate's first name:\n");
    char firstName[15];
    scanf("%s", firstName);

    int found = 0;
    int candidateId = -1;
    for (int i = 0; i < 7; i++) {
        if (strcmp(c[i].fname, firstName) == 0) {
            found = 1;
            candidateId = i;
            break;
        }
    }

    if (found == 0) {
        printf("Candidate not found.\n");
    }
    else {
        printCandidate(c[candidateId]);
    }


}

void viewElectionResults(struct electionResult result, struct candidate c[]) {
    printf("View Election Results\n");
    printf("-----------------\n");

    printf("Number of voters: %d\n", result.totalVotes);
    printf("Invalid votes: %d\n", result.invalidVotes);
    printf("Valid votes: %d\n", result.validVotes);

    printf("Candidate Score\n");

    for (int i = 0; i < 7; i++) {
        printf("%-10s %5d\n", c[i].fname, c[i].numVotes);
    }

    // print winner
    int winningScore = getMostVotes(c);
    printf("Winner(s):\n");
    for (int i = 0; i < 7; i++) {
        if (c[i].numVotes == winningScore)
        printf("\t%s %s\n", c[i].fname, c[i].lname);
    }

}

void getCandidates(FILE * file, struct candidate candidates[]) {
    for (int i = 0; i < 7; i++) {
        printf("Got a candidate\n");
        // get name
        fscanf(file, "%s", candidates[i].fname);
        fscanf(file, "%s", candidates[i].lname);

        // get campaign slogan
        int j = 0;      // tracks which letter we are inserting into the campaign slogan
        char curr;
        fscanf(file,"%c", &curr);
        // clear all leading white space
        while (curr == ' ') {
            fscanf(file,"%c", &curr);
        }

        while (curr != '\n') {
            if (curr == '\"') {
                // get next letter
                fscanf(file,"%c", &curr);
                continue;
            }
            // add letter to candidate slogan
            candidates[i].campaignSlogan[j] = curr;
            j++;

            // get next letter
            fscanf(file,"%c", &curr);
        }

        // forcibly add a trailing \0 to end of string
        // This ensures that a string gets properly created
        candidates[i].campaignSlogan[j] = '\0';


        // set the default number of votes
        candidates[i].numVotes = 0;

        printCandidate(candidates[i]);
    }
}
struct electionResult getVoteInformation(FILE *file, struct candidate candidates[]) {
    // create a electionResult struct and intialize it's default values
    struct electionResult result;
    result.invalidVotes = 0;
    result.validVotes = 0;
    result.totalVotes = 0;

    // 1. read in the vote data
    int currentVote;
    fscanf(file, "%d", &currentVote);
    while (currentVote != -1) {
        // tally the vote
        // - get the candidate (vote - 1)
        // - candidates are zero-indexed
        int candidateId = currentVote - 1;

        if (candidateId < 0 || candidateId > 7) {
            // invalid vote
            result.invalidVotes += 1;
        }
        else {
            candidates[candidateId].numVotes += 1;
            result.validVotes += 1;
        }

        // increase total vote counter
        result.totalVotes = result.totalVotes + 1;

        // read the next vote
        fscanf(file, "%d", &currentVote);
    }

    printAllCandidates(candidates);

    return result;
}

void printCandidate(struct candidate c) {
    printf("Candidate: %s %s\n", c.fname, c.lname);
    printf("Slogan: %s\n", c.campaignSlogan);
    printf("Num votes: %d\n", c.numVotes);
}

void printAllCandidates(struct candidate c[]) {
    for (int i = 0; i < 7; i++) {
        printf("Candidate: %s %s\n", c[i].fname, c[i].lname);
        printf("Slogan: %s\n", c[i].campaignSlogan);
        printf("Num votes: %d\n", c[i].numVotes);
    }
}

int getMostVotes(struct candidate c[]) {
   // what is the largest number of votes?
   int largest = 0;
   for (int i = 0; i < 7; i++) {
       int curr = c[i].numVotes;
       if (curr > largest) {
           largest = curr;
       }
   }
   return largest;


}